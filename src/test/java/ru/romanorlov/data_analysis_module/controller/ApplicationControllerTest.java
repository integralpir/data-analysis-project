package ru.romanorlov.data_analysis_module.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.romanorlov.data_analysis_module.dto.application.ohlc.OHLCIndicator;
import ru.romanorlov.data_analysis_module.exception_handling.exceptions.IncorrectMinutesValueException;
import ru.romanorlov.data_analysis_module.service.application.ApplicationServiceImpl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ApplicationControllerTest {

    @Mock
    private ApplicationServiceImpl service;

    @InjectMocks
    private ApplicationController controller;

    @Test
    void getOHLCIndicatorsByTitle_shouldCallService() {
        final OHLCIndicator indicators = mock(OHLCIndicator.class);
        when(service.calculateOHLCIndicatorByTitle("TEST", 1)).thenReturn(indicators);

        final OHLCIndicator actual = controller.getOHLCIndicatorsByTitle("TEST", 1);

        assertNotNull(actual);
        assertEquals(actual, indicators);
        assertThrows(IncorrectMinutesValueException.class,
                () -> controller.getOHLCIndicatorsByTitle("TEST", -1));
        verify(service).calculateOHLCIndicatorByTitle("TEST", 1);
    }
}
