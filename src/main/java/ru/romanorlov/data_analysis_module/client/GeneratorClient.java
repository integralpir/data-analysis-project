package ru.romanorlov.data_analysis_module.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.romanorlov.data_analysis_module.dto.generator_client.GeneratorClientCurrencyPair;

@FeignClient(name = "generator-client", url = "${feign.client.config.generator-client.url}")
public interface GeneratorClient {
    @GetMapping(value = "/pair/{pairTitle}")
    GeneratorClientCurrencyPair getPairByTitle(@PathVariable String pairTitle);
}
