package ru.romanorlov.data_analysis_module.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ru.romanorlov.data_analysis_module.dto.collector_client.CollectorCurrencyPair;

import java.util.Map;

@FeignClient(name="collector-client", url="${feign.client.config.collector-client.url}")
public interface CollectorClient {
    @GetMapping(value = "/pair/{title}")
    CollectorCurrencyPair getTodayDataOnCurrencyPair(@PathVariable String title, @RequestParam Map<String, String> parameters);
}
