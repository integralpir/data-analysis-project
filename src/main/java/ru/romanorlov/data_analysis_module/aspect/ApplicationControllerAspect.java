package ru.romanorlov.data_analysis_module.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Component
@Aspect
public class ApplicationControllerAspect {
    @Value("${server.time-zone}")
    private String TIME_ZONE_TITLE;
    private static final Logger LOGGER = LogManager.getLogger(ApplicationControllerAspect.class);
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Before("execution(* ru.romanorlov.data_analysis_module.controller.ApplicationController.getOHLCIndicators(..)) && " +
            "execution(* ru.romanorlov.data_analysis_module.controller.ApplicationController.getOHLCIndicatorsByTitle(..))")
    public void aftergetOHLCIndicatorsAdvice() {
        LOGGER.info("Controller has accepted a request for information: " + ZonedDateTime.now(ZoneId.of(TIME_ZONE_TITLE)).format(FORMATTER));
    }
}
