package ru.romanorlov.data_analysis_module.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Component
@Aspect
public class StatisticsAspect {
    @Value("${server.time-zone}")
    private String TIME_ZONE_TITLE;
    private static final Logger LOGGER = LogManager.getLogger(ApplicationControllerAspect.class);
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @AfterThrowing(pointcut = "execution(* ru.romanorlov.data_analysis_module.dto.application.statistics.ApplicationCurrencyPairStatistics.findByTitle(..))",
            throwing = "exception")
    public void afterGetTheHistoryByTitleAdvice(Throwable exception) {
        LOGGER.warn("The user was trying to get information on a non-existent currency pair: "
                + exception.getMessage() + " "
                + ZonedDateTime.now(ZoneId.of(TIME_ZONE_TITLE)).format(FORMATTER));
    }
}
