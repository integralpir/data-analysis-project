package ru.romanorlov.data_analysis_module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableFeignClients
@EnableScheduling
public class DataAnalysisModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataAnalysisModuleApplication.class, args);
    }

}
