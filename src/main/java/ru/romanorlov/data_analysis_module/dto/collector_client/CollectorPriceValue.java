package ru.romanorlov.data_analysis_module.dto.collector_client;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CollectorPriceValue {
    private double price;
    private LocalDateTime dateTime;
}
