package ru.romanorlov.data_analysis_module.dto.collector_client;

import lombok.Data;


@Data
public class CollectorCurrencyPair {
    private String title;
    private CollectorPriceValue[] values;
}
