package ru.romanorlov.data_analysis_module.dto.generator_client;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GeneratorClientPriceValue {
    private long id;
    private double price;
    private LocalDateTime date;
}
