package ru.romanorlov.data_analysis_module.dto.generator_client;

public enum GeneratorClientCurrencyPairsTitle {
    EURUSD,
    GBPUSD,
    USDJPY,
    USDCHF
}
