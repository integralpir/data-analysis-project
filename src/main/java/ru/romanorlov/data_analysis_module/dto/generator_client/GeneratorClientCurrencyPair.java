package ru.romanorlov.data_analysis_module.dto.generator_client;

import lombok.Data;

@Data
public class GeneratorClientCurrencyPair {
    private GeneratorClientCurrencyPairsTitle title;
    private GeneratorClientPriceValue[] priceHistory;
}
