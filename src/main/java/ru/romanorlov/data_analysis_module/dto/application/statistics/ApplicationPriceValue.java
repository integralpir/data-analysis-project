package ru.romanorlov.data_analysis_module.dto.application.statistics;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalTime;

@Data
@EqualsAndHashCode
@Builder
public class ApplicationPriceValue {
    private double price;
    private LocalTime time;
}
