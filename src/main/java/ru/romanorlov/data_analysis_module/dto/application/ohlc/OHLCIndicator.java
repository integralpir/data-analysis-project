package ru.romanorlov.data_analysis_module.dto.application.ohlc;

import lombok.Builder;
import lombok.Data;
import ru.romanorlov.data_analysis_module.dto.application.statistics.ApplicationPriceValue;

import java.util.List;

@Data
@Builder
public class OHLCIndicator {
    private String title;
    private int minutes;
    private OHLCColour colour;
    private double open;
    private double high;
    private double low;
    private double close;
    private List<ApplicationPriceValue> prices;
}
