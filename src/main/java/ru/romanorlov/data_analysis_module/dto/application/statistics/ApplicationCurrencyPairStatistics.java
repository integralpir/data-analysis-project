package ru.romanorlov.data_analysis_module.dto.application.statistics;

import lombok.Data;
import org.springframework.stereotype.Component;
import ru.romanorlov.data_analysis_module.exception_handling.exceptions.CannotFindPairWithTitleException;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class ApplicationCurrencyPairStatistics {
    private List<ApplicationCurrencyPair> currencyPairs = new ArrayList<>();

    public ApplicationCurrencyPair findByTitle(String title) {
        return currencyPairs.stream()
                .filter(pair -> pair.getTitle().equals(title))
                .findFirst()
                .orElseThrow( () -> new CannotFindPairWithTitleException("Could not find pair with title: " + title));
    }
}
