package ru.romanorlov.data_analysis_module.dto.application.statistics;

import lombok.Data;

import java.util.Set;
import java.util.TreeSet;


@Data
public class ApplicationCurrencyPair {
    private String title;
    private Set<ApplicationPriceValue> values;

    public ApplicationCurrencyPair(String title) {
        this.title = title;
        values = new TreeSet<>((o1, o2) -> o2.getTime().compareTo(o1.getTime()));
    }
}
