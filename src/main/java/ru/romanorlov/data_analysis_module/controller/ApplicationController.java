package ru.romanorlov.data_analysis_module.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.romanorlov.data_analysis_module.dto.application.ohlc.OHLCIndicator;
import ru.romanorlov.data_analysis_module.exception_handling.exceptions.IncorrectMinutesValueException;
import ru.romanorlov.data_analysis_module.service.application.ApplicationServiceImpl;

import java.util.List;


@RestController
@RequestMapping(value = "/ohlc")
public class ApplicationController {
    private ApplicationServiceImpl service;

    @GetMapping
    public List<OHLCIndicator> getOHLCIndicators(@RequestParam("minutes") int minutes) {
        checkMinutes(minutes);
        return service.calculateOHLCIndicator(minutes);
    }

    @GetMapping(value = "/{title}")
    public OHLCIndicator getOHLCIndicatorsByTitle(@PathVariable String title, @RequestParam("minutes") int minutes) {
        checkMinutes(minutes);
        return service.calculateOHLCIndicatorByTitle(title, minutes);
    }

    private void checkMinutes(int minutes) {
        if (minutes <= 0) {
            throw new IncorrectMinutesValueException("Please enter a value of minutes greater than 0");
        }
    }

    @Autowired
    public void setService(ApplicationServiceImpl service) {
        this.service = service;
    }
}
