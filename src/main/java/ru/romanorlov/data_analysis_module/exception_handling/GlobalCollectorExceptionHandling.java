package ru.romanorlov.data_analysis_module.exception_handling;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.romanorlov.data_analysis_module.exception_handling.exceptions.CannotFindPairWithTitleException;
import ru.romanorlov.data_analysis_module.exception_handling.exceptions.IncorrectMinutesValueException;
import ru.romanorlov.data_analysis_module.exception_handling.exceptions.InsufficientInformationException;

@ControllerAdvice
public class GlobalCollectorExceptionHandling {

    @ExceptionHandler
    public ResponseEntity<ExceptionData> handleException(IncorrectMinutesValueException exception) {
        return new ResponseEntity<>(createExceptionData(exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionData> handleException(CannotFindPairWithTitleException exception) {
        return new ResponseEntity<>(createExceptionData(exception.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionData> handleException(InsufficientInformationException exception) {
        return new ResponseEntity<>(createExceptionData(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ExceptionData createExceptionData(String message) {
        ExceptionData data = new ExceptionData();
        data.setInfo(message);

        return data;
    }
}
