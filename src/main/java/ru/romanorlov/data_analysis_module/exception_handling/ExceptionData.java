package ru.romanorlov.data_analysis_module.exception_handling;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ExceptionData {
    private String info;
}
