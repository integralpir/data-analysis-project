package ru.romanorlov.data_analysis_module.exception_handling.exceptions;

public class InsufficientInformationException extends RuntimeException{
    public InsufficientInformationException(String message) {
        super(message);
    }
}
