package ru.romanorlov.data_analysis_module.exception_handling.exceptions;

public class IncorrectMinutesValueException extends RuntimeException{
    public IncorrectMinutesValueException(String message) {
        super(message);
    }
}
