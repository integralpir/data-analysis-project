package ru.romanorlov.data_analysis_module.exception_handling.exceptions;

public class CannotFindPairWithTitleException extends RuntimeException{
    public CannotFindPairWithTitleException(String message) {
        super(message);
    }
}
