package ru.romanorlov.data_analysis_module.service.collector_client;

import javax.annotation.PostConstruct;

public interface CollectorClientService {
    @PostConstruct
    void uploadDataToStatistics();
}
