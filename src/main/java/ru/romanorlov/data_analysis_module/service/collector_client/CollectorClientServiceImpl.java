package ru.romanorlov.data_analysis_module.service.collector_client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.romanorlov.data_analysis_module.client.CollectorClient;
import ru.romanorlov.data_analysis_module.dto.application.statistics.ApplicationCurrencyPair;
import ru.romanorlov.data_analysis_module.dto.application.statistics.ApplicationCurrencyPairStatistics;
import ru.romanorlov.data_analysis_module.dto.application.statistics.ApplicationPriceValue;
import ru.romanorlov.data_analysis_module.dto.collector_client.CollectorCurrencyPair;
import ru.romanorlov.data_analysis_module.dto.collector_client.CollectorPriceValue;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;


@Service
public class CollectorClientServiceImpl implements CollectorClientService {
    @Value("${currency.pairs}")
    private String[] currencyPairsTitles;
    @Value("${server.time-zone}")
    private String TIME_ZONE_TITLE;
    private final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private CollectorClient client;
    private ApplicationCurrencyPairStatistics statistics;

    @Override
    @PostConstruct
    public void uploadDataToStatistics(){
        try {
            LocalDateTime now = ZonedDateTime.now(ZoneId.of(TIME_ZONE_TITLE)).toLocalDateTime();
            LocalDateTime startOfTheDay = LocalDateTime.of(now.getYear(), now.getMonth(), now.getDayOfMonth(), 0, 0, 0);

            String toDate = DATE_FORMATTER.format(now);
            String fromDate = DATE_FORMATTER.format(startOfTheDay);

            Arrays.stream(currencyPairsTitles)
                    .forEach(title -> {
                        ApplicationCurrencyPair applicationPair = new ApplicationCurrencyPair(title);
                        CollectorCurrencyPair collectorPair = getDataFromClient(title, fromDate, toDate);

                        Set<ApplicationPriceValue> applicationValues = applicationPair.getValues();
                        CollectorPriceValue[] collectorValues = collectorPair.getValues();

                        Arrays.stream(collectorValues)
                                .map(this::convertClientValueToApplicationValue)
                                .forEach(applicationValues::add);

                        statistics.getCurrencyPairs().add(applicationPair);
                    });
        } catch (Throwable ignored) {
            uploadDataToStatistics();
        }
    }

    private CollectorCurrencyPair getDataFromClient(String title, String fromDate, String toDate) {
        return client.getTodayDataOnCurrencyPair(title, new HashMap<>() {{
            put("fromDate", fromDate);
            put("toDate", toDate);
        }});
    }

    private ApplicationPriceValue convertClientValueToApplicationValue(CollectorPriceValue clientValue) {
        LocalDateTime dateTime = clientValue.getDateTime();

        return ApplicationPriceValue.builder()
                .price(clientValue.getPrice())
                .time(LocalTime.of(dateTime.getHour(), dateTime.getMinute(), dateTime.getSecond(), dateTime.getNano()))
                .build();
    }

    @Autowired
    public void setClient(CollectorClient client) {
        this.client = client;
    }

    @Autowired
    public void setStatistics(ApplicationCurrencyPairStatistics statistics) {
        this.statistics = statistics;
    }
}
