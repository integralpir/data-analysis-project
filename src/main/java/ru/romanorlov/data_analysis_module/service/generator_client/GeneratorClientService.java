package ru.romanorlov.data_analysis_module.service.generator_client;

import org.springframework.scheduling.annotation.Scheduled;

public interface GeneratorClientService {
    @Scheduled(fixedDelay = 5000)
    void uploadDataToStatistics();
}
