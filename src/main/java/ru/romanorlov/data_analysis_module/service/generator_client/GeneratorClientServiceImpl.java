package ru.romanorlov.data_analysis_module.service.generator_client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.romanorlov.data_analysis_module.client.GeneratorClient;
import ru.romanorlov.data_analysis_module.dto.application.statistics.ApplicationCurrencyPairStatistics;
import ru.romanorlov.data_analysis_module.dto.application.statistics.ApplicationPriceValue;
import ru.romanorlov.data_analysis_module.dto.generator_client.GeneratorClientCurrencyPair;
import ru.romanorlov.data_analysis_module.dto.generator_client.GeneratorClientPriceValue;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Set;

@Service
public class GeneratorClientServiceImpl implements GeneratorClientService{
    @Value("${server.time-zone}")
    private String TIME_ZONE_TITLE;
    private GeneratorClient client;
    private ApplicationCurrencyPairStatistics statistics;

    @Override
    @Scheduled(initialDelay = 5000, fixedDelay = 5000)
    public void uploadDataToStatistics() {
        statistics.getCurrencyPairs()
                .forEach(applicationPair -> {
                    GeneratorClientCurrencyPair clientPair = client.getPairByTitle(applicationPair.getTitle());

                    Set<ApplicationPriceValue> applicationValues = applicationPair.getValues();
                    GeneratorClientPriceValue[] clientValues = clientPair.getPriceHistory();

                    Arrays.stream(clientValues)
                            .filter(value -> value.getDate().isAfter(ZonedDateTime.now(ZoneId.of(TIME_ZONE_TITLE)).toLocalDateTime()))
                            .map(this::convertClientValueToApplicationValue)
                            .forEach(applicationValues::add);
                });
    }

    private ApplicationPriceValue convertClientValueToApplicationValue(GeneratorClientPriceValue clientValue) {
        LocalDateTime dateTime = clientValue.getDate();

        return ApplicationPriceValue.builder()
                .price(clientValue.getPrice())
                .time(LocalTime.of(dateTime.getHour(), dateTime.getMinute(), dateTime.getSecond(), dateTime.getNano()))
                .build();
    }

    @Autowired
    public void setStatistics(ApplicationCurrencyPairStatistics statistics) {
        this.statistics = statistics;
    }

    @Autowired
    public void setClient(GeneratorClient client) {
        this.client = client;
    }
}
