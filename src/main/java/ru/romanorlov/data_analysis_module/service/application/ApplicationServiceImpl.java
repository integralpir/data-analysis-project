package ru.romanorlov.data_analysis_module.service.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.romanorlov.data_analysis_module.dto.application.ohlc.OHLCColour;
import ru.romanorlov.data_analysis_module.dto.application.ohlc.OHLCIndicator;
import ru.romanorlov.data_analysis_module.dto.application.statistics.ApplicationCurrencyPair;
import ru.romanorlov.data_analysis_module.dto.application.statistics.ApplicationCurrencyPairStatistics;
import ru.romanorlov.data_analysis_module.dto.application.statistics.ApplicationPriceValue;
import ru.romanorlov.data_analysis_module.exception_handling.exceptions.InsufficientInformationException;

import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApplicationServiceImpl implements ApplicationService{
    @Value("${server.time-zone}")
    private String TIME_ZONE_TITLE;
    private ApplicationCurrencyPairStatistics statistics;

    @Override
    public List<OHLCIndicator> calculateOHLCIndicator(int minutes) {
        return statistics.getCurrencyPairs().stream()
                .map(pair -> calculateOHLCIndicatorByTitle(pair.getTitle(), minutes))
                .collect(Collectors.toList());
    }

    @Override
    public OHLCIndicator calculateOHLCIndicatorByTitle(String title, int minutes) {
        LocalTime now = ZonedDateTime.now(ZoneId.of(TIME_ZONE_TITLE)).toLocalTime();
        LocalTime startTime = now.minusMinutes(minutes);

        ApplicationCurrencyPair currencyPair = statistics.findByTitle(title);

        List<ApplicationPriceValue> values = currencyPair.getValues().stream()
                .filter(value -> value.getTime().isAfter(startTime) && value.getTime().isBefore(now))
                .collect(Collectors.toList());

        if (values.isEmpty()) {
            throw new InsufficientInformationException("So far, we do not have the necessary amount of information");
        } else {
            return OHLCIndicator.builder()
                    .title(title)
                    .open(values.get(0).getPrice())
                    .high(values.stream()
                            .map(ApplicationPriceValue::getPrice)
                            .max(Double::compareTo)
                            .get())
                    .low(values.stream()
                            .map(ApplicationPriceValue::getPrice)
                            .min(Double::compareTo)
                            .get())
                    .close(values.get(values.size() - 1).getPrice())
                    .colour(values.get(0).getPrice() < values.get(values.size() - 1).getPrice() ? OHLCColour.GREEN : OHLCColour.RED)
                    .minutes(minutes)
                    .prices(values)
                    .build();
        }
    }

    @Override
    @Scheduled(cron = "0 0 0 * * *", zone = "${server.time-zone}")
    public void clearHistory() {
        statistics.getCurrencyPairs()
                .forEach(pair -> pair.getValues().clear());
    }

    @Autowired
    public void setStatistics(ApplicationCurrencyPairStatistics statistics) {
        this.statistics = statistics;
    }
}
