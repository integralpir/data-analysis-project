package ru.romanorlov.data_analysis_module.service.application;

import org.springframework.scheduling.annotation.Scheduled;
import ru.romanorlov.data_analysis_module.dto.application.ohlc.OHLCIndicator;

import java.util.List;

public interface ApplicationService {
    List<OHLCIndicator> calculateOHLCIndicator(int minutes);
    OHLCIndicator calculateOHLCIndicatorByTitle(String title, int minutes);
    @Scheduled(cron = "0 0 0 * * *", zone = "${server.time-zone}")
    void clearHistory();
}
